﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models.Project
{
    public class Contact_DTO
    {
        public Int64 Con_PkeyID { get; set; }
        public Int64? Con_UserID { get; set; }
        public String Con_Name { get; set; }
        public String Con_Email { get; set; }
        public String Con_Phone { get; set; }
        public String Con_Website { get; set; }
        public String Con_Message { get; set; }
        public Boolean? Con_IsActive { get; set; }
        public Boolean? Con_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserID { get; set; }
    }
}