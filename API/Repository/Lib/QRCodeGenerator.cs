﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZXing;
using ZXing.Common;
using Avigma.Models;
namespace Avigma.Repository.Lib
{
    public class QRCodeGenerator
    {
        public string GenerateQRCode(QRCodeModelDTO qRCodeModelDTO)
        {
            Log log = new Log();
            string imagePath = string.Empty;
            try
            {
                string imageextension = "";
                string folderPath = System.Configuration.ConfigurationManager.AppSettings["QRImagePath"];
                string QRImagePathImageDBPath = System.Configuration.ConfigurationManager.AppSettings["QRImagePathImageDBPath"];
                imagePath = folderPath + "/" + qRCodeModelDTO.QRCodeText;
                // If the directory doesn't exist then create it.
                //if (!Directory.Exists(HttpContext.Current.Server.MapPath(folderPath)))
                //{
                //    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(folderPath));
                //}

                BarcodeWriter barcodeWriter = new BarcodeWriter();

                barcodeWriter.Format = BarcodeFormat.QR_CODE;
                var result = barcodeWriter.Write(qRCodeModelDTO.QRCodeText);

                imageextension = ".jpg";
                var newfileName = Guid.NewGuid() + imageextension;
                imagePath = QRImagePathImageDBPath + newfileName;
                //string barcodePath = HttpContext.Current.Server.MapPath(imagePath);
                string barcodePath = folderPath + "\\" + newfileName ;
                var barcodeBitmap = new Bitmap(result);
                using (MemoryStream memory = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(barcodePath, FileMode.Create, FileAccess.ReadWrite))
                    {
                        barcodeBitmap.Save(memory, ImageFormat.Jpeg);
                        byte[] bytes = memory.ToArray();
                        fs.Write(bytes, 0, bytes.Length);
                    }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
     
            return imagePath;
        }
    }
}