﻿using Avigma.Repository.Lib;
using Avigma.Repository.Security;
using API.Models.Project;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Avigma.Models;

namespace API.Repository.Project
{
    public class Contact_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        SecurityHelper securityHelper = new SecurityHelper();

        private List<dynamic> CreateUpdate_Contact(Contact_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Contact]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Con_PkeyID", 1 + "#bigint#" + model.Con_PkeyID);
                input_parameters.Add("@Con_UserID", 1 + "#bigint#" + model.Con_UserID);
                input_parameters.Add("@Con_Name", 1 + "#varchar#" + model.Con_Name);
                input_parameters.Add("@Con_Email", 1 + "#nvarchar#" + model.Con_Email);
                input_parameters.Add("@Con_Phone", 1 + "#nvarchar#" + model.Con_Phone);
                input_parameters.Add("@Con_Website", 1 + "#nvarchar#" + model.Con_Website);
                input_parameters.Add("@Con_Message", 1 + "#varchar#" + model.Con_Message);
                input_parameters.Add("@Con_IsActive", 1 + "#bit#" + model.Con_IsActive);
                input_parameters.Add("@Con_IsDelete", 1 + "#bit#" + model.Con_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Con_PkeyID_Out ", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        private DataSet Get_Contact(Contact_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Contact]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Con_PkeyID", 1 + "#bigint#" + model.Con_PkeyID);
                input_parameters.Add("@Con_UserID", 1 + "#bigint#" + model.Con_UserID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }
        public List<dynamic> CreateUpdateContact_DataDetails(Contact_DTO model)
        {
            EmailTemplate emailTemplate = new EmailTemplate();
            EmailDTO emailDTO = new EmailDTO();
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdate_Contact(model);
                if (objData != null)
                {
                    emailDTO.To = model.Con_Email;
                    emailDTO.MainBody = model.Con_Message;
                    emailTemplate.NewUserRegister(emailDTO, "", 0);
                }


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }
        public List<dynamic> Get_ContactDetails(Contact_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = Get_Contact(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Contact_DTO> Get_details =
                   (from item in myEnumerableFeaprd
                    select new Contact_DTO
                    {
                        Con_PkeyID = item.Field<Int64>("Con_PkeyID"),
                        Con_UserID = item.Field<Int64?>("Con_UserID"),
                        Con_Name = item.Field<String>("Con_Name"),
                        Con_Email = item.Field<String>("Con_Email"),
                        Con_Phone = item.Field<String>("Con_Phone"),
                        Con_Website = item.Field<String>("Con_Website"),
                        Con_Message = item.Field<String>("Con_Message"),
                        Con_IsActive = item.Field<Boolean?>("Con_IsActive"),
                    }).ToList();

                objDynamic.Add(Get_details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;





        }

    }
}