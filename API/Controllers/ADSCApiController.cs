﻿using Avigma.Repository.Lib;
using HospitalMangement.Models.Project;
using HospitalMangement.Repository.Project;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace HospitalMangement.Controllers
{
    [RoutePrefix("api/ADSCApi")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ADSCApiController : ApiController
    {
        Log log = new Log();
        ADSC_Data aDSC_Data = new ADSC_Data();
        DepressionScreeningTool_Client_Data depressionScreeningTool_Client_Data = new DepressionScreeningTool_Client_Data();
        ModifiedSprint_Client_Data modifiedSprint_Client_Data = new ModifiedSprint_Client_Data();
        MoodDisorderQuestionnaire_Client_Data moodDisorderQuestionnaire_Client_Data = new MoodDisorderQuestionnaire_Client_Data();


        // for AnxietyDisorderScreen_Client
        [HttpPost]
        [Route("AddADSC_Details")]
        public async Task<List<dynamic>> AddADSC_Details()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {

                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var aDSC_DTO = JsonConvert.DeserializeObject<ADSC_DTO>(s);


                var AddADSC_Details = await Task.Run(() => aDSC_Data.AddADSC_Data(aDSC_DTO));

                return AddADSC_Details;
            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return objdynamicobj;
            }
        }

        [HttpPost]
        [Route("Get_AnxietyDisorderScreen_Client_Data")]
        public async Task<List<dynamic>> Get_AnxietyDisorderScreen_Client_Data()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {

                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var aDSC_DTO = JsonConvert.DeserializeObject<ADSC_DTO>(s);


                var AddADSC_Details = await Task.Run(() => aDSC_Data.Get_AnxietyDisorderScreen_ClientDetails(aDSC_DTO));

                return AddADSC_Details;
            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return objdynamicobj;
            }
        }

        //for DepressionScreeningTool_Client
        [HttpPost]
        [Route("AddDepressionScreeningTool_Client_Details")]
        public async Task<List<dynamic>> AddDepressionScreeningTool_Client_Details()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {

                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var depressionScreeningTool_Client_DTO = JsonConvert.DeserializeObject<DepressionScreeningTool_Client_DTO>(s);


                var AddDepressionScreeningTool_Client_Details = await Task.Run(() => depressionScreeningTool_Client_Data.AddDepressionScreeningTool_Client_Data(depressionScreeningTool_Client_DTO));

                return AddDepressionScreeningTool_Client_Details;
            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return objdynamicobj;
            }
        }

        
        [HttpPost]
        [Route("Get_DepressionScreeningTool_ClientData")]
        public async Task<List<dynamic>> Get_DepressionScreeningTool_ClientData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {

                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var depressionScreeningTool_Client_DTO = JsonConvert.DeserializeObject<DepressionScreeningTool_Client_DTO>(s);


                var AddDepressionScreeningTool_Client_Details = await Task.Run(() => depressionScreeningTool_Client_Data.Get_DepressionScreeningTool_ClientDetails(depressionScreeningTool_Client_DTO));

                return AddDepressionScreeningTool_Client_Details;
            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return objdynamicobj;
            }
        }

        // for Modified sprint client

        [HttpPost]
        [Route("AddModifiedSprint_Client_Details")]
        public async Task<List<dynamic>> AddModifiedSprint_Client_Details()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {

                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var modifiedSprint_Client_DTO = JsonConvert.DeserializeObject<ModifiedSprint_Client_DTO>(s);


                var AddModifiedSprint_Client_Details = await Task.Run(() => modifiedSprint_Client_Data.AddModifiedSprint_Client_Data(modifiedSprint_Client_DTO));

                return AddModifiedSprint_Client_Details;
            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return objdynamicobj;
            }
        }

        [HttpPost]
        [Route("Get_ModifiedSprint_ClientData")]
        public async Task<List<dynamic>> Get_ModifiedSprint_ClientData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {

                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var modifiedSprint_Client_DTO = JsonConvert.DeserializeObject<ModifiedSprint_Client_DTO>(s);


                var ModifiedSprint_ClientData = await Task.Run(() => modifiedSprint_Client_Data.Get_ModifiedSprint_ClientDetails(modifiedSprint_Client_DTO));

                return ModifiedSprint_ClientData;
            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return objdynamicobj;
            }
        }

        // for MoodDisorderQuestionnaire_Client

        [HttpPost]
        [Route("AddMoodDisorderQuestionnaire_Client_Details")]
        public async Task<List<dynamic>> AddMoodDisorderQuestionnaire_Client_Details()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {

                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var moodDisorderQuestionnaire_Client_DTO = JsonConvert.DeserializeObject<MoodDisorderQuestionnaire_Client_DTO>(s);


                var MoodDisorderQuestionnaire_Client_Details = await Task.Run(() => moodDisorderQuestionnaire_Client_Data.AddMoodDisorderQuestionnaire_Client_Data(moodDisorderQuestionnaire_Client_DTO));

                return MoodDisorderQuestionnaire_Client_Details;
            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return objdynamicobj;
            }
        }

        [HttpPost]
        [Route("Get_MoodDisorderQuestionnaire_ClientData")]
        public async Task<List<dynamic>> Get_MoodDisorderQuestionnaire_ClientData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {

                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var moodDisorderQuestionnaire_Client_DTO = JsonConvert.DeserializeObject<MoodDisorderQuestionnaire_Client_DTO>(s);


                var MoodDisorderQuestionnaire_ClientData = await Task.Run(() => moodDisorderQuestionnaire_Client_Data.Get_MoodDisorderQuestionnaire_ClientDetails(moodDisorderQuestionnaire_Client_DTO));

                return MoodDisorderQuestionnaire_ClientData;
            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return objdynamicobj;
            }
        }


    }
}
