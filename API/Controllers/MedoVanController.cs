﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;
using API.Models.Project;
using API.Repository.Project;
using API.Repository.Lib;
using Avigma.Models;
using API.Models;

namespace API.Controllers
{
    [RoutePrefix("api/MedoVan")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MedoVanController : BaseController
    {
        Log log = new Log();
        Contact_Data contact_Data = new Contact_Data();
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("AddUserMasterData")]
        public async Task<List<dynamic>> AddUserMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {

                UserMaster_Data userMaster_Data = new UserMaster_Data();

                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<UserMaster_DTO>(s);
                Data.UserID = LoggedInUserId;
                if (Data.Type != 1)
                {
                    //Data.User_PkeyID = LoggedInUserId;
                }
                var userMasterDetails = await Task.Run(() => userMaster_Data.AddUserMaster_Data(Data));

                return userMasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetUserMasterData")]
        public async Task<List<dynamic>> GetUserMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                UserMaster_Data userMaster_Data = new UserMaster_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<UserMaster_DTO>(s);
                Data.UserID = LoggedInUserId;
                Data.User_PkeyID = LoggedInUserId;
                var userMasterDetails = await Task.Run(() => userMaster_Data.Get_UserMasterDetails(Data));

                return userMasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [HttpPost]
        [Route("ForGotPassword")]
        public async Task<List<dynamic>> ForGotPassword()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                GetSetUser getSetUser = new GetSetUser();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------ForGotPassword Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------ForGotPassword End--------------");
                var user_Child_DTO = JsonConvert.DeserializeObject<UserLogin>(s);


                var MasterDetails = await Task.Run(() => getSetUser.GetForGetPassword(user_Child_DTO));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [HttpPost]
        [Route("GetUserViryficationDetails")]
        public async Task<List<dynamic>> GetUserViryficationDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                UserVerificationMaster_Data userVerificationMaster_Data = new UserVerificationMaster_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------GetUserViryficationDetails Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetUserViryficationDetails End--------------");
                //log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<UserVerificationMaster_DTO>(s);
                //Data.Type = 1;

                var Getuser = await Task.Run(() => userVerificationMaster_Data.Check_User(Data));
                return Getuser;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("ChangePassword")]
        public async Task<List<dynamic>> ChangePassword()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                UserMaster_Data userMaster_Data = new UserMaster_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------ChangePassword Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------ChangePassword End--------------");
                var userMaster_DTO = JsonConvert.DeserializeObject<UserMaster_ChangePassword>(s);

                var userDetails = await Task.Run(() => userMaster_Data.ChangePassword(userMaster_DTO));
                userMaster_DTO.Type = 5;
                return userDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("CreateContact")]
        public async Task<List<dynamic>> CreateContact()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Contact_DTO>(s);
                Data.UserID = LoggedInUserId;

                var CreateUpdateContact_DataDetails = await Task.Run(() => contact_Data.CreateUpdateContact_DataDetails(Data));

                return CreateUpdateContact_DataDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("CreateContact");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetContact")]
        public async Task<List<dynamic>> GetContact()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Contact_DTO>(s);
                Data.UserID = LoggedInUserId;

                var Get_ContactDetails = await Task.Run(() => contact_Data.Get_ContactDetails(Data));

                return Get_ContactDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetContact");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

    }
}