﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;
using HospitalMangement.Models.Project;
using HospitalMangement.Repository.Project;
using API.Repository.Lib;
using Avigma.Models;
using API.Repository.Project;
using API.Models.Project;

namespace HospitalMangement.Controllers
{
    [RoutePrefix("api/HealthyMindsApp")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HealthyMindsAppController : BaseController
    {
        Log log = new Log();
        NDSD_Data nDSD_Data = new NDSD_Data();

        [Authorize]
        [HttpPost]
        [Route("GetMasterDetails")]
        public async Task<List<dynamic>> GetMasterDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var nDSD_DTO = JsonConvert.DeserializeObject<NDSD_DTO>(s);
                //nDSD_DTO.UserID = LoggedInUserId;
                nDSD_DTO.Type = 1;

                var MasterDetails = await Task.Run(() => nDSD_Data.Get_NDSD_MasterDetails(nDSD_DTO));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("PostPatientDetails")]
        public async Task<List<dynamic>> PostPatientDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                PatientData patientData = new PatientData();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<PatientMaster>(s);
                //Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => patientData.AddPatientData(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetPatientDetails")]
        public async Task<List<dynamic>> GetPatientDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                PatientData patientData = new PatientData();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<Healthy_Minds_Philly_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => nDSD_Data.Get_patientDetails(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("PostSurveryQuestions")]
        public async Task<List<dynamic>> PostSurveryQuestions()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                Survey_Questions_Master_Data Survey_Questions_Master_Data = new Survey_Questions_Master_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<Survey_Questions_Master_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => Survey_Questions_Master_Data.AddUPdateSurvey_Questions_Master(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetSurveryQuestions")]
        public async Task<List<dynamic>> GetSurveryQuestions()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                Survey_Questions_Master_Data Survey_Questions_Master_Data = new Survey_Questions_Master_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<Survey_Questions_Master_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => Survey_Questions_Master_Data.Get_Survey_Questions_MasterDetails(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostSurveryResponse")]
        public async Task<List<dynamic>> PostSurveryResponse()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                Survey_Customer_Response_Master_Data survey_Customer_Response_Master_Data = new Survey_Customer_Response_Master_Data(); 
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<Customer_Response_Master_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => survey_Customer_Response_Master_Data.AddUpdateSurvey_Customer_Response_Master_Data(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetResponseData")]
        public async Task<List<dynamic>> GetResponseData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                Survey_Customer_Response_Master_Data survey_Customer_Response_Master_Data = new Survey_Customer_Response_Master_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<Survey_Customer_Response_Master_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => survey_Customer_Response_Master_Data.Get_Survey_Customer_Response_MasterDetails(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostHealthCenterData")]
        public async Task<List<dynamic>> PostHealthCenterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                HealthCenterData healthCenterData = new HealthCenterData();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<HealthCenterDTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => healthCenterData.Insert_Update_HealthCenter(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetHealthCenterData")]
        public async Task<List<dynamic>> GetHealthCenterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                HealthCenterData healthCenterData = new HealthCenterData();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<HealthCenterDTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => healthCenterData.Get_HealthCenter_Data(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostTIPSData")]
        public async Task<List<dynamic>> PostTIPSData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                TipsTricksData tipsTricksData = new TipsTricksData();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<TipsTricksDTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => tipsTricksData.Insert_Update_TipsMaster(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetTIPSData")]
        public async Task<List<dynamic>> GetTIPSData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                TipsTricksData tipsTricksData = new TipsTricksData();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<TipsTricksDTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => tipsTricksData.Get_TIPS_Data(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetHealthCenterDataWithDist")]
        public async Task<List<dynamic>> GetHealthCenterDataWithDist()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                HealthCenterData healthCenterData = new HealthCenterData();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<HealthCenterDTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => healthCenterData.Get_HealthCenter_Data(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("AddUserMasterData")]
        public async Task<List<dynamic>> AddUserMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                UserMaster_Data userMaster_Data = new UserMaster_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<UserMaster_DTO>(s);
                Data.UserID = LoggedInUserId;
                if (Data.Type != 1)
                {
                    Data.User_PkeyID = LoggedInUserId;
                }
                var userMasterDetails = await Task.Run(() => userMaster_Data.AddUserMaster_Data(Data));

                return userMasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetUserMasterData")]
        public async Task<List<dynamic>> GetUserMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                UserMaster_Data userMaster_Data = new UserMaster_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<UserMaster_DTO>(s);
                Data.UserID = LoggedInUserId;
                Data.User_PkeyID = LoggedInUserId;
                var userMasterDetails = await Task.Run(() => userMaster_Data.Get_UserMasterDetails(Data));

                return userMasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [HttpPost]
        [Route("ForGotPassword")]
        public async Task<List<dynamic>> ForGotPassword()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                GetSetUser getSetUser = new GetSetUser();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------ForGotPassword Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------ForGotPassword End--------------");
                var user_Child_DTO = JsonConvert.DeserializeObject<UserLogin>(s);
              

                var MasterDetails = await Task.Run(() => getSetUser.GetForGetPassword(user_Child_DTO));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }




        [HttpPost]
        [Route("GetUserViryficationDetails")]
        public async Task<List<dynamic>> GetUserViryficationDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                UserVerificationMaster_Data userVerificationMaster_Data = new UserVerificationMaster_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------GetUserViryficationDetails Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------GetUserViryficationDetails End--------------");
                //log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<UserVerificationMaster_DTO>(s);
                //Data.Type = 1;

                var Getuser = await Task.Run(() => userVerificationMaster_Data.Check_User(Data));
                return Getuser;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("ChangePassword")]
        public async Task<List<dynamic>> ChangePassword()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                UserMaster_Data userMaster_Data = new UserMaster_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------ChangePassword Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------ChangePassword End--------------");
                var userMaster_DTO = JsonConvert.DeserializeObject<UserMaster_ChangePassword>(s);

                var userDetails = await Task.Run(() => userMaster_Data.ChangePassword(userMaster_DTO));
                userMaster_DTO.Type = 5;
                return userDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostADSCData")]
        public async Task<List<dynamic>> PostADSCData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                ADSC_Data aDSC_Data = new ADSC_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<ADSC_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => aDSC_Data.AddADSC_Data(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetADSCData")]
        public async Task<List<dynamic>> GetADSCData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                ADSC_Data aDSC_Data = new ADSC_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<ADSC_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => aDSC_Data.Get_AnxietyDisorderScreen_ClientDetails(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("PostDepressionScreeningToolClientData")]
        public async Task<List<dynamic>> PostDepressionScreeningToolClientData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                DepressionScreeningTool_Client_Data depressionScreeningTool_Client_Data = new DepressionScreeningTool_Client_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<DepressionScreeningTool_Client_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => depressionScreeningTool_Client_Data.AddDepressionScreeningTool_Client_Data(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetDepressionScreeningToolClientData")]
        public async Task<List<dynamic>> GetDepressionScreeningToolClientData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                DepressionScreeningTool_Client_Data depressionScreeningTool_Client_Data = new DepressionScreeningTool_Client_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<DepressionScreeningTool_Client_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => depressionScreeningTool_Client_Data.Get_DepressionScreeningTool_ClientDetails(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AddModifiedSprintClientData")]
        public async Task<List<dynamic>> AddModifiedSprintClientData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                ModifiedSprint_Client_Data modifiedSprint_Client_Data = new ModifiedSprint_Client_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<ModifiedSprint_Client_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => modifiedSprint_Client_Data.AddModifiedSprint_Client_Data(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetModifiedSprintClientData")]
        public async Task<List<dynamic>> GetModifiedSprintClientData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                ModifiedSprint_Client_Data modifiedSprint_Client_Data = new ModifiedSprint_Client_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<ModifiedSprint_Client_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => modifiedSprint_Client_Data.Get_ModifiedSprint_ClientDetails(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AddMoodDisorderQuestionnaireClientData")]
        public async Task<List<dynamic>> AddMoodDisorderQuestionnaireClientData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                MoodDisorderQuestionnaire_Client_Data moodDisorderQuestionnaire_Client_Data = new MoodDisorderQuestionnaire_Client_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<MoodDisorderQuestionnaire_Client_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => moodDisorderQuestionnaire_Client_Data.AddMoodDisorderQuestionnaire_Client_Data(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetMoodDisorderQuestionnaireClientData")]
        public async Task<List<dynamic>> GetMoodDisorderQuestionnaireClientData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                MoodDisorderQuestionnaire_Client_Data moodDisorderQuestionnaire_Client_Data = new MoodDisorderQuestionnaire_Client_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<MoodDisorderQuestionnaire_Client_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => moodDisorderQuestionnaire_Client_Data.Get_MoodDisorderQuestionnaire_ClientDetails(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("AddTreatedForData")]
        public async Task<List<dynamic>> AddTreatedForData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                Treated_For_Data treated_For_Data = new Treated_For_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<Treated_For_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => treated_For_Data.AddTreated_For_Data(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("GetTreatedForData")]
        public async Task<List<dynamic>> GetTreatedForData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                Treated_For_Data treated_For_Data = new Treated_For_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<Treated_For_DTO>(s);
                Data.UserID = LoggedInUserId;

                var MasterDetails = await Task.Run(() => treated_For_Data.Get_Treated_ForDetails(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Pradeep Yadav

        //[Authorize]
        [HttpPost]
        [Route("AddBlogsMasterData")]
        public async Task<List<dynamic>> AddBlogsMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                BlogsMaster_Data blogsMaster_Data = new BlogsMaster_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<BlogsMaster_DTO>(s);
                Data.Blog_UserId = LoggedInUserId;

                var MasterDetails = await Task.Run(() => blogsMaster_Data.AddBlogsMaster_Data(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [HttpPost]
        [Route("GetBlogsMasterData")]
        public async Task<List<dynamic>> GetBlogsMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                BlogsMaster_Data blogsMaster_Data = new BlogsMaster_Data();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<BlogsMaster_DTO>(s);
                Data.Blog_UserId = LoggedInUserId;

                var MasterDetails = await Task.Run(() => blogsMaster_Data.Get_BlogsMaster_Details(Data));

                return MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //[AllowAnonymous]
        //[Authorize]
        [HttpPost]
        [Route("UploadPhotosData")]
        public async Task<string> UploadPhotosData()
        {
            ImageGenerator imageGenerator = new ImageGenerator();
            string strimg = string.Empty;

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------UploadPhotosData  Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("-----------UploadPhotosData  End----------------");
                var data = JsonConvert.DeserializeObject<BlogsMaster_DTO>(s);
                string imgPath = data.Blog_ImagePath;
                strimg = await Task.Run(() => imageGenerator.Base64ToImage(imgPath));

                return strimg;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("UploadPhotosData");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

                return strimg;
            }
        }

    }
}
